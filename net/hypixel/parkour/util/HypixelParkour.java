package net.hypixel.parkour.util;

import net.hypixel.parkour.events.JumpListener;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class HypixelParkour {

    private Plugin plugin;
    private int players, max_players, phase;
    public List<Player> playerlist;
    private boolean isStarted, isStopped;

    public HypixelParkour(Plugin plugin, int maxPlayers) {
        this.plugin = plugin;
        this.players = 0;
        this.playerlist = new ArrayList<>();
        this.phase = 0;
        this.isStarted = false;
        this.isStopped = false;
        this.max_players = maxPlayers;
    }

    public void timer() {
        final int[] sec = {169};
        final Location[] winner = new Location[1];
        new BukkitRunnable() {
            @Override
            public void run() {
                sec[0]++;
                if(sec[0] == 60) timerTick(120);
                if(sec[0] == 120) timerTick(60);
                if(sec[0] == 150) timerTick(30);
                if(sec[0] == 165) timerTick(15);
                if(sec[0] == 170) timerTick(10);
                if(sec[0] == 175) timerTick(5);
                if(sec[0] == 176) timerTick(4);
                if(sec[0] == 177) timerTick(3);
                if(sec[0] == 178) timerTick(2);
                if(sec[0] == 179) timerTick(1);
                if(sec[0] == 180) {
                    isStopped = true;
                    Iterator it = JumpListener.scores.entrySet().iterator();
                    int i1 =0;
                    String s1 = "";
                    while (it.hasNext()) {
                        Map.Entry pair = (Map.Entry) it.next();
                        if (i1 < (int) pair.getValue()) {
                            i1 = (int) pair.getValue();
                            Player p = Bukkit.getPlayer((UUID) pair.getKey());
                            s1 = p.getName();
                            winner[0] = p.getLocation();
                        }
                    }
                    Bukkit.broadcastMessage("§7"+s1+"§e won the game with §a"+i1+" §epoints!");
                }
                if(sec[0] > 180) {
                    Firework fw = (Firework) winner[0].getWorld().spawnEntity(winner[0], EntityType.FIREWORK);
                    FireworkMeta fwm = fw.getFireworkMeta();
                    fwm.addEffect(FireworkEffect.builder().flicker(true).trail(true).withColor(Color.YELLOW).withColor(Color.LIME).build());
                    fw.setFireworkMeta(fwm);
                }
                if(sec[0] == 190) {
                    Bukkit.shutdown();
                }
            }
        }.runTaskTimer(plugin, 0, 20);
    }

    public void timerTick(int sec) {
        for(Player p : Bukkit.getOnlinePlayers()) {
            p.sendMessage("§c"+sec+" seconds remaining!");
            p.playSound(p.getLocation(), Sound.CLICK,1,1);
        }
    }


    public Plugin getPlugin() {
        return plugin;
    }
    public int getPlayers() {
        return players;
    }
    public int getMax_players() {
        return max_players;
    }
    public int getPhase() {
        return phase;
    }
    public boolean isStarted() {
        return isStarted;
    }


    public void starterTimer() {
        starterTimerP(5);
    }

    private void starterTimerP(int i) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                if(playerlist.size() == max_players) {
                    if(i > 0) {
                        Bukkit.broadcastMessage("§eThe game starts in §c"+i+"§e!");
                        for(Player p : playerlist) p.playSound(p.getLocation(), Sound.CLICK, 1, 1);
                        starterTimerP(i-1);
                    } else {
                        timer();
                        isStarted = true;
                    }
                }
            }
        }, 20);
    }

    public boolean isStopped() {
        return isStopped;
    }
}
