package net.hypixel.parkour.util;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Team;

import java.util.*;

public class Scoreboard {


    public static Map<Player, List<String>> oldobjectives = new HashMap<Player, List<String>>();
    public static Map<Player, org.bukkit.scoreboard.Scoreboard> boards = new HashMap<Player, org.bukkit.scoreboard.Scoreboard>();
    public static Map<UUID, List<String>> scoreboards = new HashMap<>();

    public static void onScoreboard() {
        for(Player p : Bukkit.getOnlinePlayers()) {
            if(!(boards.containsKey(p))){
                org.bukkit.scoreboard.Scoreboard board = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
                boards.put(p, board);
            }
            org.bukkit.scoreboard.Scoreboard board = boards.get(p);            if(!oldobjectives.containsKey(p)) oldobjectives.put(p, new ArrayList<String>());
            if(scoreboards.containsKey(p.getUniqueId()))   {
                if(!oldobjectives.get(p).equals(scoreboards.get(p.getUniqueId()))) {
                    oldobjectives.put(p, scoreboards.get(p.getUniqueId()));
                    Objective objective;
                    if(board.getObjective(p.getName()) != null) board.getObjective(p.getName()).unregister();
                    objective = board.registerNewObjective(p.getName(), "dummy");
                    objective.setDisplayName("§e§lPARKOUR");
                    int s = scoreboards.get(p.getUniqueId()).size();
                    for(String st : scoreboards.get(p.getUniqueId())) {
                        Score sc = objective.getScore(st.replaceAll("&", "§"));s--;
                        sc.setScore(s);
                    }
                    objective.setDisplaySlot(DisplaySlot.SIDEBAR);
                    p.setScoreboard(board);
                }
            }
        }
    }
}
