package net.hypixel.parkour;

import net.hypixel.parkour.events.JumpListener;
import net.hypixel.parkour.events.PlayerJoinLeave;
import net.hypixel.parkour.util.HypixelParkour;
import net.hypixel.parkour.util.Scoreboard;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Score;

import java.text.SimpleDateFormat;
import java.util.*;

public class Main extends JavaPlugin {

    public static HypixelParkour hypixelParkour;

    @Override
    public void onEnable() {
        System.out.println("[INFO] HypixelParkour Enabled");
        hypixelParkour = new HypixelParkour(this, 1);

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new PlayerJoinLeave(), this);
        pm.registerEvents(new JumpListener(), this);

        new BukkitRunnable() {
            @Override
            public void run() {
                Iterator it = JumpListener.scores.entrySet().iterator();
                int st1 = 0, th2 = 0, th3 = 0, th4 = 0, th5 = 0;
                String sst1 = "", sth2 = "", sth3 = "", sth4= "", sth5 = "";

                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    if(st1 < (int) pair.getValue()) {
                        st1 = (int) pair.getValue();
                        sst1 = Bukkit.getPlayer((UUID) pair.getKey()).getName();
                    }
                }
                for(Player p : Bukkit.getOnlinePlayers()) {
                    if(hypixelParkour.isStarted()) {
                        Date date = new Date();
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/YYYY");
                        List<String> list = new ArrayList<>();
                        list.add("&7"+formatter.format(date));
                        list.add("  ");
                        list.add("&eYour score: &a"+JumpListener.scores.get(p.getUniqueId()));
                        list.add("   ");
                        if(st1 != 0) list.add("&7"+sst1+"&e: &a"+st1);
                        if(th2 != 0) list.add("&7"+sth2+"&e: &a"+th2);
                        if(th3 != 0) list.add("&7"+sth3+"&e: &a"+th3);
                        if(th4 != 0) list.add("&7"+sth4+"&e: &a"+th4);
                        if(th5 != 0) list.add("&7"+sth5+"&e: &a"+th5);
                        list.add("    ");
                        list.add("&eplay.hypixel.net");
                        Scoreboard.scoreboards.remove(p.getUniqueId());
                        Scoreboard.scoreboards.put(p.getUniqueId(), list);
                    }
                }
                Scoreboard.onScoreboard();
            }
        }.runTaskTimer(this, 0, 20);
    }
}
