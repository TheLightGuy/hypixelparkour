package net.hypixel.parkour.events;

import net.hypixel.parkour.Main;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerJoinLeave implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        event.setJoinMessage("");
        if(Main.hypixelParkour.isStarted()) {
            event.getPlayer().kickPlayer("Game already started.");
        } else {
            Main.hypixelParkour.playerlist.add(event.getPlayer());
            Bukkit.broadcastMessage("§7"+event.getPlayer().getName()+" §ehas joined (§b"+Main.hypixelParkour.playerlist.size()+"§e/§b"+Main.hypixelParkour.getMax_players()+"§e)!");
            if(Main.hypixelParkour.playerlist.size() == Main.hypixelParkour.getMax_players()) Main.hypixelParkour.starterTimer();
        }

    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        event.setQuitMessage("");
        if(Main.hypixelParkour.playerlist.contains(event.getPlayer())) {
            Main.hypixelParkour.playerlist.remove(event.getPlayer());
            if (!Main.hypixelParkour.isStarted()) Bukkit.broadcastMessage("§7"+event.getPlayer().getName()+" §ehas left (§b"+Main.hypixelParkour.playerlist.size()+"§e/§b"+Main.hypixelParkour.getMax_players()+"§e)!");
        }
    }
}
