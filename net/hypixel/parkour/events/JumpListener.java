package net.hypixel.parkour.events;

import net.hypixel.parkour.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.*;

public class JumpListener implements Listener {

    private Map<Player, Double> blockfalls = new HashMap<>();

    public static Map<UUID, Integer> scores = new HashMap<>();

    private Map<Player, List<Location>> blocktypes = new HashMap<>();

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Location loc = event.getPlayer().getLocation();
        double xt, yt, zt, xf, yf, zf;
        xf = event.getFrom().getX();
        yf = event.getFrom().getY();
        zf = event.getFrom().getZ();
        xt = event.getTo().getX();
        yt = event.getTo().getY();
        zt = event.getTo().getZ();
        if(xf == xt && yf == yt && zf == zt) return;
        if(Main.hypixelParkour.isStarted() && !Main.hypixelParkour.isStopped()) {
            if((int) yf > (int) yt) {
                double i = yf-yt;
                if(blockfalls.containsKey(event.getPlayer())) i = i + blockfalls.get(event.getPlayer());
                blockfalls.put(event.getPlayer(), i);
                if(i > 4) {
                    event.getPlayer().teleport(new Location(loc.getWorld(), 529, 80, 163));
                    blockfalls.remove(event.getPlayer());
                    if(blocktypes.containsKey(event.getPlayer())) for(Location l : blocktypes.get(event.getPlayer())) event.getPlayer().sendBlockChange(l, Material.STAINED_CLAY, (byte) 4);
                    blocktypes.put(event.getPlayer(), new ArrayList<>());
                    return;
                }
            }
            Block block = loc.clone().add(0,-1,0).getBlock();
            if(block.getType() == Material.STAINED_CLAY) {
                List<Location> list = new ArrayList<>();
                if(blocktypes.containsKey(event.getPlayer())) list = blocktypes.get(event.getPlayer());
                if(!list.contains(block.getLocation())) {
                    list.add(block.getLocation());
                    event.getPlayer().sendBlockChange(block.getLocation(), Material.STAINED_CLAY, (byte) 13);
                    event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ORB_PICKUP,1, 1);
                    blocktypes.put(event.getPlayer(), list);
                    int score = list.size();
                    int ls = 0;
                    if(scores.containsKey(event.getPlayer().getUniqueId())) ls = scores.get(event.getPlayer().getUniqueId());
                    if(score > ls) {
                        scores.remove(event.getPlayer().getUniqueId());
                        scores.put(event.getPlayer().getUniqueId(), score);
                    }
                }
            }
        }
    }


}
